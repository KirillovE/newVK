//
//  NewsStruct.swift
//  WatchExtension Extension
//
//  Created by Евгений Кириллов on 03.06.2018.
//  Copyright © 2018 Триада. All rights reserved.
//

import Foundation

struct NewsStruct {
    let author, text, avatar, image, day, time: String
}
